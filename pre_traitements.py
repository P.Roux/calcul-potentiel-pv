import psycopg2
import os
import sys
import rasterio
import geopandas
import pandas as pd
from math import *
import unidecode
from interpolation_lidar import *

def existe_batiment_dans_mns(rep) :
    """ Vérifie si le MNS contient des bâtiments (inutiles de lancer l'interpolation si ce n'est pas le cas).
    """
    cur = conn.cursor()
    req = "select b.id from occ_sol.bati b join lidar.source_donnees_lidar_petr a on st_intersects(a.geom, b.geom) where a.nom_pkk = '{rep}';".format(rep=rep)
    cur.execute(req)
    liste_donnees_lidar = [i[0] for i in cur.fetchall()]
    if not liste_donnees_lidar :
        return False
    return True

def rep_traite(rep) :
    """ Vérifie si le répertoire contenant les données LIDAR a été traité.
    """
    if rep in os.listdir("./rep_traites") :
        return True
    return False

def ajouter_rep_traite(rep) :
    """ Ajoute le répertoire à la liste des répertoires traités (fichier portant son nom dans le répertoire rep_traites).
    """
    with open("./rep_traites/{rep}".format(rep=rep), "x") as fichier :
        print("Le dossier de fichiers LAZ {rep} a bien été traité.".format(rep=rep))

def postgis_vers_shp(schema, table) :
    """ Convertit une table de données géographiques postgresql en shp
    """
    print("""
              pgsql2shp -f {dest_tmp} -h {hote} -u {utilisateur} -P {mdp} -g geom {bdd} "SELECT c.* FROM {schema}.{table} c join lidar.source_donnees_lidar_petr p on st_intersects(c.geom, p.geom) where p.nom_pkk = '{rep}';"
              """.format(
                    bdd=bdd,
                    utilisateur=utilisateur,
                    mdp=mdp,
                    hote=hote,
                    schema=schema,
                    table=table,
                    rep=commune.replace("'", "''"),
                    dest_tmp=dest_tmp,
              ))
    os.system("""
              pgsql2shp -f {dest_tmp} -h {hote} -u {utilisateur} -P {mdp} -g geom {bdd} "SELECT c.* FROM {schema}.{table} c join lidar.source_donnees_lidar_petr p on st_intersects(c.geom, p.geom) where p.nom_pkk = '{rep}';"
              """.format(
                    bdd=bdd,
                    utilisateur=utilisateur,
                    mdp=mdp,
                    hote=hote,
                    schema=schema,
                    table=table,
                    rep=commune.replace("'", "''"),
                    dest_tmp=dest_tmp,
              ))
# def postgis_vers_shp_commune(schema, table) :
#     """
#     """
#     print("""
#               pgsql2shp -f {dest_tmp} -h {hote} -u {utilisateur} -P {mdp} -g geom {bdd} "select id, geom from lidar.source_donnees_lidar_petr where nom_pkk = '{commune}' ;"
#               """.format(
#                     bdd=bdd,
#                     utilisateur=utilisateur,
#                     mdp=mdp,
#                     hote=hote,
#                     schema=schema,
#                     table=table,
#                     commune=commune.replace("'", "''"),
#                     dest_tmp=dest_tmp_commune,
#               ))
#     os.system("""
#               pgsql2shp -f {dest_tmp} -h {hote} -u {utilisateur} -P {mdp} -g geom {bdd} "select id, geom from lidar.source_donnees_lidar_petr where nom_pkk = '{commune}' ;"
#               """.format(
#                     bdd=bdd,
#                     utilisateur=utilisateur,
#                     mdp=mdp,
#                     hote=hote,
#                     schema=schema,
#                     table=table,
#                     commune=commune.replace("'", "''"),
#                     dest_tmp=dest_tmp_commune,
#               ))

def decouper_raster_masque(masque, raster, dest) :
    """ Extrait un raster selon une couche de masque.
    """
    print("""gdalwarp -overwrite -cutline {shp} -crop_to_cutline {raster} {dest}""".format(
            shp=masque,
            raster=raster,
            dest=dest
    ))
    os.system("""gdalwarp -overwrite -cutline {shp} -crop_to_cutline {raster} {dest}""".format(
            shp=masque,
            raster=raster,
            dest=dest
    ))

def decouper_selon_emprise(raster_masque, raster_a_decouper) :
    """Découpe un raster selon une emprise.
    """
    emprise = rasterio.open(raster_masque).bounds
    os.system("gdal_translate -projwin {gauche} {haut} {droite} {bas} -of GTiff {toitures} {toitures_decoupees}".format(
            gauche=emprise.left,
            bas=emprise.bottom,
            droite=emprise.right,
            haut=emprise.top,
            toitures=dest,
            toitures_decoupees=toitures_decoupees
    ))
    os.system("rm {toitures}".format(toitures=dest))

def filtrer_raster(raster_or, raster_dest, ranger=15) :
    """ Applique un filtre avec OTB afin d'éliminer le bruit du MNS et homogénéiser les zones en prévision de la segmentation.
    """
    os.system("otbcli_MeanShiftSmoothing -in {toitures} -fout {toitures_filtrees} float -ranger {ranger} -ram 256".format(toitures=raster_or, toitures_filtrees=raster_dest, ranger=ranger))

def exposition_mns() :
    """Calcul l'exposition du MNS.
    """
    os.system("gdaldem aspect {toitures_filtrees} {exposition_toitures} -of GTiff -b 1".format(toitures_filtrees=toitures_filtrees, exposition_toitures=exposition_toitures))

def pente_mns() :
    """ Calcul la pente du MNS
    """
    os.system("gdaldem slope {toitures_filtrees} {pente_toitures} -of GTiff -b 1 -s 1.0".format(toitures_filtrees=toitures_filtrees, pente_toitures=pente_toitures))

def segmentation(image_filtree, image_segmentee, ranger=15 ) :
    """Segmente une image filtrée pour séparer les pans de toitures
    """
    os.system("otbcli_LSMSSegmentation -in {filtree} -out {segmente} uint32 -ranger {ranger}".format(filtree=image_filtree, segmente=image_segmentee, ranger=ranger))

def simplifier_segmentation() :
    """ Fusionne les petites zones non utilisables pour le calcul du potentiel PV
    """
    os.system("otbcli_LSMSSmallRegionsMerging -in {exposition_toitures} -inseg {exposition_toitures_filtrees_segmentees} -out {exposition_toitures_filtrees_segmentees_simplifiees} uint32 -ram 256".format(exposition_toitures=exposition_toitures, exposition_toitures_filtrees_segmentees=exposition_toitures_filtrees_segmentees, exposition_toitures_filtrees_segmentees_simplifiees=exposition_toitures_filtrees_segmentees_simplifiees))

def vectoriser(image, image_segmentee, image_vectorisee) :
    """Convertit l'image segmentée en vecteur
    """
    os.system("otbcli_LSMSVectorization -in {exposition_toitures} -inseg {exposition_toitures_filtrees_segmentees_simplifiees} -out {toitures_vectorisees} -ram 256".format(exposition_toitures=image, exposition_toitures_filtrees_segmentees_simplifiees=image_segmentee, toitures_vectorisees=image_vectorisee))

def statistiques_de_zone(fichier_dest, fichier_or, fichier_donnees, colonnes, typestat) :
    """Pour chaque entité d'un vecteur, ajoute un champ correspondant à un calcul statistique du raster sur leur emprise géographique.
    Par exemple, calculer la moyenne d'exposition pour chaque entité issue des toitures vectorisées extraites du MNS.
    """
    os.system("qgis_process run native:zonalstatisticsfb --distance_units=meters --area_units=m2 --ellipsoid=EPSG:7019 --INPUT={fichier_or} --INPUT_RASTER={fichier_donnees} --RASTER_BAND=1 --COLUMN_PREFIX={prefix} --STATISTICS={typestat} --OUTPUT={fichier_sortie}".format(fichier_dest=fichier_dest, fichier_or=fichier_or, fichier_donnees=fichier_donnees, prefix=colonnes, fichier_sortie=fichier_dest, typestat=typestat))

def conversion_intervalle(intervalle) :
   l = [intervalle.split("-")[0].replace("°", ""), intervalle.split("-")[1].replace("°", "")]
   return list(range(int(l[0]), int(l[1])))

def calcul_potentiel_pv(pente, exposition, surface, irr) :
    """Calcule le potentiel PV
    """
    chemin_donnees = './donnees/table-potentiel-solaire.csv'
    df = pd.read_csv(chemin_donnees, sep=";") # Table permettant de calculer le facteur de correction.
    df["inclinaison"] = df["Inclinaison"].apply(conversion_intervalle)
    df["orientation"] = df["Orientation"].apply(conversion_intervalle)
    dffc = df[(df["inclinaison"].apply(lambda interval: int(pente) in interval)) & (df["orientation"].apply(lambda interval: abs(180 - int(exposition)) in interval))]
    fc = 0 # Facteur de correction : si aucun des critères retenus (trop pentu ou exposé nord),
           # le facteur de production est nul et le potentiel est égal à 0.
    if len(dffc["Correction"]) != 0 :
        fc = float(dffc["Correction"])
    s_dispo = 0.7
    if pente <= 5 : # Si le toit est considéré comme plat (seuil modifiable)
        s_dispo = 0.55
    s = s_dispo * surface
    return s * irr * 8.766 * fc * 0.75 * 0.16

def calcul_potentiel_pv_gdf() :
    """ Applique la fonction de calcul du potentiel PV à chaque ligne d'un GéoDataFrame
    """
    gdf = geopandas.read_file(fichier_dest_final).fillna(0)
    gdf["surface"] = gdf["surf_sum"]
    gdf = gdf[gdf["surface"] < 10000] # Suppression des surfaces ne pouvant correspondre à une toiture
    gdf["potentiel_pv"] = gdf.apply(lambda x: calcul_potentiel_pv(gdf.at[x.name, "pente_mean"], gdf.at[x.name, "expo_mean"], gdf.at[x.name, "surface"], gdf.at[x.name, "irr_mean"]), axis=1)
    gdf.to_file(toitures_pv_finales)

def calcul_surfaces_reelles() :
    """Calcule les surfaces réelles et non uniquement celle de l'emprise au sol.
    """
    os.system("""
    qgis_process run sagang:realsurfacearea --distance_units=meters --area_units=m2 --ellipsoid=EPSG:7019 --DEM={toitures_filtrees} --AREA={surfaces_reelles}
    """.format(
        toitures_filtrees=toitures_filtrees,
        surfaces_reelles=surfaces_reelles
    ))

def filtrer_valeurs_vecteur(vecteur, valeur, signe, colonne, destination) :
    """Filtrer les lignes considérées non pertinentes (surfaces réelles mal calculées du fait d'une pente  100% par exemple).
    """
    gdf = geopandas.read_file(vecteur)
    if signe == "=" :
        gdf = gdf[gdf[colonne] == valeur]
    elif signe == ">" :
        gdf = gdf[gdf[colonne] > valeur]
    elif signe == "<" :
        gdf = gdf[gdf[colonne] < valeur]
    elif signe == ">=" :
        gdf = gdf[gdf[colonne] >= valeur]
    elif signe == "<=" :
        gdf = gdf[gdf[colonne] <= valeur]
    gdf.to_file(destination)

if __name__ == "__main__" :
    l_communes = liste_toute_donnee_lidar()
    for commune in l_communes :
        if not rep_traite(commune) :
            if existe_batiment_dans_mns(commune) or not existe_batiment_dans_mns(commune):
                bdd = "plateforme_consultation_phr"
                mdp = "Bozouls12340"
                utilisateur = "phr"
                hote = "localhost"
                print("Début du programme pour le répertoire {rep}... Il est composé de 20 étapes".format(rep=commune))
                print("1) Création du MNS pour le répertoire {commune}".format(commune=commune))
                dossier_mns = "/media/paul/TOSHIBA EXT/donnees-lidar-est-petr"
                dossier_mns_laz = "/media/paul/TOSHIBA EXT/donnees-lidar-est-petr/{rep}".format(rep=commune)
                if "mns_{commune}.tif".format(commune=commune) not in os.listdir("./mns") :
                    interpolation_lidar_rep(dossier_mns_laz, 0.2)
                dest_raster = "./sorties/toitures-filtrees-{commune}.tif".format(commune=commune)
                dest = "./sorties/toitures-{commune}.tif".format(commune=commune)
                toitures_decoupees = "./sorties/toitures-decoupees-{commune}.tif".format(commune=commune)
                toitures_filtrees = "./sorties/toitures-filtrees-{commune}.tif".format(commune=commune)
                dest_tmp = "./tmp/bati-{commune}.shp".format(commune=commune)
                dest_tmp_commune = "./tmp/masque-{commune}.shp".format(commune=commune)
                raster = "/home/paul/calcul-potentiel-pv/mns/mns_{commune}.tif".format(commune=commune)
                exposition_toitures = "./sorties/exposition-toitures-{commune}.tif".format(commune=commune)
                exposition_toitures_filtrees = "./sorties/exposition-toitures-filtrees-{commune}.tif".format(commune=commune)
                exposition_toitures_filtrees_segmentees = "./sorties/exposition-toitures-filtrees-segmentees-{commune}.tif".format(commune=commune)
                exposition_toitures_filtrees_segmentees_simplifiees = "./sorties/exposition-toitures-filtrees-segmentees-simplifiee-{commune}.tif".format(commune=commune)
                toitures_vectorisees = "./sorties/toitures-vectorisees-{commune}.shp".format(commune=commune)
                pente_toitures = "./sorties/pente-toitures-{commune}.tif".format(commune=commune)
                pente_toitures_decoupees = "./sorties/pente-toitures-decoupees-{commune}.tif".format(commune=commune)
                exposition_toitures_decoupees = "./sorties/exposition-toitures-decoupees-{commune}.tif".format(commune=commune)
                fichier_dest_irradiation = "./tmp/toitures_irradiations.shp"
                fichier_dest_irradiation_pente = "./tmp/toitures_irradiations_pentes.shp"
                fichier_dest_final = "./sorties/toitures_donnees_finales.shp"
                fichier_dest_irradiation_pente_exposition = "./tmp/toitures_irradiations_pentes_exposition.shp"
                raster_irradiation = "./donnees/irradiation-globale-france.tif"
                toitures_pv_finales = "./sorties_finales/toitures_pv_finales_{commune}.shp".format(commune=commune)
                surfaces_reelles = "./calculs-surfaces-reelles/surfaces_reelles.sdat"
                surfaces_reelles_decoupees = "./calculs-surfaces-reelles/surfaces_reelles_decoupees.tif"
                surfaces_reelles_filtrees = "./calculs-surfaces-reelles/surfaces_reelles_filtrees.tif"
                surfaces_reelles_segmentees = "./calculs-surfaces-reelles/surfaces_reelles_segmentees.tif"
                surfaces_reelles_vectorisees = "./calculs-surfaces-reelles/surfaces_reelles_vectorisees.shp"
                surfaces_reelles_vectorisees_filtrees = "./calculs-surfaces-reelles/surfaces_reelles_vectorisees_filtrees.shp"
                print("2) Extraction du bâti...")
                table = "bati"
                # Possibilité d'utiliser ces lignes de code pour ne calculer le potentiel que sur les parcelles publiques
                # if sys.argv[1] == "public" :
                #     table = "bati_parcelles_publiques"
                postgis_vers_shp("occ_sol", table)
                print("3) Extraire les toitures...")
                decouper_raster_masque(dest_tmp, raster, dest)
                print("4) Appliquer un filtre sur les toitures...")
                filtrer_raster(dest, toitures_filtrees)
                print("5) Calcul de l'exposition à partir des toitures filtrées...")
                exposition_mns()
                print("6) Appliquer un filtre sur l'exposition des toitures...")
                filtrer_raster(exposition_toitures, exposition_toitures_filtrees)
                print("7) Segmenter le raster d'exposition des toitures...")
                segmentation(exposition_toitures_filtrees, exposition_toitures_filtrees_segmentees)
                print("8) Fusionner les petites régions (simplification de la segmentation)...")
                simplifier_segmentation()
                print("9) Vectoriser ces segments...")
                vectoriser(exposition_toitures, exposition_toitures_filtrees_segmentees_simplifiees, toitures_vectorisees)
                print("Calcul des surfaces réelles depuis le MNS des toitures. Suppression des murs dont on ne veut pas calculer les surfaces car ils provoquent des biais")
                print("10) Calcul de la pente des toitures...")
                pente_mns()
                print("11) Calcul des surfaces réelles...")
                calcul_surfaces_reelles()
                print("12) Filtrer sur les surfaces réelles...")
                filtrer_raster(surfaces_reelles, surfaces_reelles_filtrees, 5)
                print("13) Segmenter les surfaces réelles")
                segmentation(surfaces_reelles_filtrees, surfaces_reelles_segmentees, 5)
                print("14) Vectoriser les surfaces réelles")
                vectoriser(surfaces_reelles, surfaces_reelles_segmentees, surfaces_reelles_vectorisees)
                print("15) Filtre sur les pixels où la pente est trop forte, ce qui biaise les calculs de surface sur les données issues de la segmentation des toitures...")
                filtrer_valeurs_vecteur(surfaces_reelles_vectorisees, 1, "<=", "meanB0", surfaces_reelles_vectorisees_filtrees)
                print("16) Découper les surfaces réelles...")
                decouper_raster_masque(surfaces_reelles_vectorisees_filtrees, surfaces_reelles, surfaces_reelles_decoupees)
                print("17) Découper les pentes à partir des surfaces réelles dont on a supprimé les surfaces trop pentues...")
                decouper_raster_masque(surfaces_reelles_vectorisees_filtrees, pente_toitures, pente_toitures_decoupees)
                print("18) Découper l'exposition des toitures à partir des surfaces réelles dont on a supprimé les surfaces trop pentues")
                decouper_raster_masque(surfaces_reelles_vectorisees_filtrees, exposition_toitures_filtrees, exposition_toitures_decoupees)
                print("19) Calcul des statistiques de zone dont on a besoin pour calculer le potentiel PV...")
                statistiques_de_zone(fichier_dest_irradiation, toitures_vectorisees, raster_irradiation, "irr_", 2)
                statistiques_de_zone(fichier_dest_irradiation_pente, fichier_dest_irradiation, pente_toitures_decoupees, "pente_", 2)
                statistiques_de_zone(fichier_dest_irradiation_pente_exposition, fichier_dest_irradiation_pente, exposition_toitures_decoupees, "expo_", 2)
                statistiques_de_zone(fichier_dest_final, fichier_dest_irradiation_pente_exposition, surfaces_reelles_decoupees, "surf_", 1)
                print("20) Calculer le potentiel PV...")
                calcul_potentiel_pv_gdf()
                print("21) Nettoyage des fichiers temporaires...")
                os.system("rm -rf ./sorties/*")
                os.system("rm -rf ./calcul_surfaces_reelles/*")
                os.system("rm -rf ./tmp/*")
            else :
                print("Les données LIDAR du répertoire {rep} ne contiennent pas de bâtiments".format(rep=commune))
            ajouter_rep_traite(commune)
        else :
            print("Ces données ont déjà été traitées")
