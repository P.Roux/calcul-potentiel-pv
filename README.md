# calcul-potentiel-pv



## Présentation

Ce script permet de calculer le potentiel de production photovoltaïque sur tous les pans de toiture des bâtiments d'une zone d'étude. Il repose pour cela sur les scripts OTB et les données LIDAR de
l'IGN.

- Algorithmes QGIS et extensions GRASS, GDAL et SAGA obligatoires
- Machine LINUX obligatoire
- PostgreSQL / PostGIS nécessaires

## Télécharger OrfeoToolBox
Télécharger OrfeoToolBox. Une fois l'archive copiée où vous le souhaitez sur votre machine,
lancez source ~/OTB-8.1.1-Linux64/otbenv.profile (modifiez OTB-8.1.1-Linux64 par le nom de votre
répertoire OTB).

## Télécharger les données LIDAR

Le script telecharger_pa_partir_csv.py permet de télécharger toutes les données LIDAR que contient
un fichier csv dont le modèle est indiqué dans le répertoire donnees.

Veillez à ne télécharer que celles dont vous avez besoin car ce processus est très long.

## Installer les dépendances Python

Il n'y a pas de fichier contenant les dépendances à exécuter pour en automatiser l'installation...
À faire !

## Installer la base de données

Celle-ci doit comprendre a minima une couche PostGIS contenant les urls et noms des tuiles LIDAR
disponibles (à télécharger sur le site de l'IGN) ainsi que les contours de la zone sur laquelle
vous souhaitez faire porter l'étude. Il faut également y intégrer les bâtiments de la BD Topo de l'IGN. Pensez ensuite à modifier le code pour en adapter les paramètres (connexion à la base de données, tables sur lesquelles portent les requêtes).

## Exécuter l'algorithme

Exécutez python3 pre_traitements.py en dehors d'un environnement virtuel (je n'ai pas su
lier qgis_process avec un environnement virtuel Python).

## Étapes de caclul du potentiel PV
**Pour chaque répertoire de données LIDAR à traiter :**
- Interpolation des données LIDAR en un modèle numérique de surface (triangulation de Delaunay mais méthode modifiable même si celle-ci semble la plus adaptée).
- Extraction des toitures du modèle numérique de surface
- Filtre sur ces toitures
- Calcul de la pente et de l'exposition
- Segmentation des toitures extraites
- Simplification de cette segmentation
- Vectorisation des toitures
- Calcul des surfaces réelles en tenant compte de la pente
- Filtre sur la pente, segmentation puis vectorisation
- Suppression des pixels pour lesquels le calcul de la surface réelle est surestimé compte-tenu d'une pente trop forte (un mur vertical par exemple, qu'on n'équipe pas en PV)
- Découpage des pentes par les surfaces réelles vectorisées afin d'éliminer les pentes trop fortes qui biaisent ensuite la moyenne (en prévision des statistiques de zones)
- Même opération pour l'exposition des toitures
- Calcul des statistiques de zone afin qu'à chaque entité des toitures vectorisées, on y associe une exposition, une surface, et une pente.
- Calcul du potentiel PV (méthode de calcul de l'ORCAE facilement personnalisable avec l'éditeur de fonction de QGIS de la calculatrice de champs une fois la données acquise).

**On obtient en sortie un fichier shp issu de la segmentation et vectorisation des toitures extraites
du MNS modélisant les pans de toiture et leur potentiel de production annuel.**
