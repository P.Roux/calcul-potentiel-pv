import pandas as pd
import urllib.request
import os

def telecharger(rep, url) :
    if url.split("/")[-1] not in os.listdir(rep) :
        print("Télécharger ", url.split("/")[-1], " ...")
        urllib.request.urlretrieve(url, rep + "/" + url.split("/")[-1])
    else :
        print("Ce fichier est déjà téléchargé")
   urllib.request.urlretrieve(url, rep + "/" + url.split("/")[-1])

df = pd.read_csv("/media/sf_partage_ubuntu/lidar-a-telecharger.csv", sep=";")
print("Il y a " + str(len(list(df["url_telech"])))  + " fichiers à télécharger...")
df.apply(lambda x : telecharger("/media/sf_partage_ubuntu/reste-donnees-lidar", df.at[x.name, "url_telech"]), axis=1)
