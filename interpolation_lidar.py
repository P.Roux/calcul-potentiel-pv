import psycopg2 as pg
import os
import unidecode
from whitebox.whitebox_tools import WhiteboxTools
# Paramètres à remplacer
conn = pg.connect(
        user = "phr",
        password = "Bozouls12340",
        host = "localhost",
        port = "5432",
        database = "plateforme_consultation_phr"
)


def donnees_lidar_manquantes_petr(dossier_mns) :
    cur = conn.cursor()
    req = "select a.nom_pkk as url from limites_administratives.perimetre_petr b, lidar.source_donnees_lidar_petr a where st_intersects(a.geom, b.geom);"
    cur.execute(req)
    liste_donnees_lidar = [i[0] for i in cur.fetchall()]
    differences = set(liste_donnees_lidar) - set(os.listdir(dossier_mns))
    return list(differences)

def liste_donnees_lidar(commune, zone=False) :
    cur = conn.cursor()
    req = "select a.nom_pkk as url from limites_administratives.perimetre_petr b, lidar.source_donnees_lidar_petr a where st_intersects(a.geom, b.geom) and b.nom = '{commune}';".format(commune=commune)
    if zone :
        req = "select a.nom_pkk as url from {zone} b, lidar.source_donnees_lidar_petr a where st_intersects(a.geom, b.geom);".format(zone=zone)
    cur.execute(req)
    liste = [i[0] for i in cur.fetchall()]
    print("Conversion en MNS des données LIDAR suivantes : ", liste)
    return liste

def liste_toute_donnee_lidar() :
    cur = conn.cursor()
    # Modifier la requête afin de sélectionner les données LIDAR correspondant à la zone d'étude
    req = "select a.nom_pkk as url from limites_administratives.perimetre_petr b, lidar.source_donnees_lidar_petr a where st_intersects(a.geom, b.geom) ;"
    # if zone :
    #     req = "select a.nom_pkk as url from {zone} b, lidar.source_donnees_lidar_petr a where st_intersects(a.geom, b.geom);".format(zone=zone)
    cur.execute(req)
    liste = [i[0] for i in cur.fetchall()]
    # print("Conversion en MNS des données LIDAR suivantes : ", liste)
    return liste

def interpolation_lidar_rep(dossier_laz, resolution) :
    """Interpole les données LIDAR (préalablement téléchargées)
    """
    os.system("mkdir ./mns/tmp")
    rep_destination_mns_tmp = "/home/paul/calcul-potentiel-pv/mns/tmp"
    wbt = WhiteboxTools()
    wbt.set_verbose_mode(True) # Sets verbose mode. If verbose mode is False, tools will not print output messages
    wbt.set_compress_rasters(True)
    wbt.set_working_dir(dossier_laz)
    for fichier in os.listdir(dossier_laz) :
        chemin_final = dossier_laz + "/" + fichier
        if os.path.isfile(chemin_final) :
            if "{fichier}.tif".format(fichier=fichier) not in os.listdir("./mns/tmp") and "copc" not in "{fichier}.tif".format(fichier=fichier).split("."):
                wbt.lidar_tin_gridding(parameter="elevation",
                    returns="last", # A DEM or DTM is usually obtained from the "last" returns, a DSM uses "first" returns (or better, use the lidar_digital_surface_model tool)
                    resolution=resolution, # This is the spatial resolution of the output raster in meters and should depend on application needs and point density.
                    minz=None,
                    maxz=None,
                    i=fichier,
                    output="{rep_destination_mns_tmp}/{fichier}.tif".format(rep_destination_mns_tmp=rep_destination_mns_tmp, fichier=fichier)
                )
        elif not os.path.isfile(chemin_final) and "laz_tmp" not in fichier.split(".") :
            print("{fichier} n'est pas un fichier mais un répertoire".format(fichier=fichier))
            wbt.set_working_dir(chemin_final)
            for f in os.listdir(chemin_final) :
                if os.path.isfile(chemin_final + "/" + f) :
                    if "laz" in "{fichier}.tif".format(fichier=f).split(".") :
                        if "{f}.tif".format(f=f) not in os.listdir("./mns/tmp") and "copc" not in "{fichier}.tif".format(fichier=f).split(".") :
                            wbt.lidar_tin_gridding(parameter="elevation",
                                returns="last", # A DEM or DTM is usually obtained from the "last" returns, a DSM uses "first" returns (or better, use the lidar_digital_surface_model tool)
                                resolution=resolution, # This is the spatial resolution of the output raster in meters and should depend on application needs and point density.
                                minz=None,
                                maxz=None,
                                i=f,
                                output="{rep_destination_mns_tmp}/{f}.tif".format(rep_destination_mns_tmp=rep_destination_mns_tmp, f=f)
                            )
    cmd_fusion_mns = "gdal_merge.py ./mns/tmp/*.tif -o ./mns/mns_{commune}.tif".format(commune=dossier_laz.split("/")[-1])
    # cmd_src_mns = "qgis_process run gdal:assignprojection --distance_units=meters --area_units=m2 --ellipsoid=EPSG:7030 --INPUT=./mns/mns-final-{commune}.tif --CRS='EPSG:2154'".format(commune=commune)
    cmd_src_mns = "gdal_edit.py -a_srs EPSG:2154 ./mns/mns_{commune}.tif".format(commune=dossier_laz.split("/")[-1])
    os.system(cmd_fusion_mns)
    os.system(cmd_src_mns)
    os.system("rm -rf ./mns/tmp/")
